import os
import subprocess

docker_image = 'bitbucketpipelines/demo-pipe-python:ci' + os.getenv('BITBUCKET_BUILD_NUMBER', 'local')

def docker_build():
  """
  Build the docker image for tests.
  :return:
  """
  args = [
    'docker',
    'build',
    '-t',
    docker_image,
    '.',
  ]
  subprocess.run(args, check=True)


def setup():
  docker_build()

def test_no_parameters():
  args = [
    'docker',
    'run',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert result.returncode == 1
  print(result.stdout)
  assert '✖ Validation errors: \nAPI_KEY:\n- required field\nAPI_URL:\n- required field\nCURRENT_BRANCH:\n- required field\nRECENT_COMMIT_MESSAGE:\n- required field' in result.stdout

def test_success():
  args = [
    'docker',
    'run',
    '-e', 'API_KEY=yp5DAQjEg4Zu1uRyMtLyf9vr0vNzqUxp',
    '-e', 'API_URL=https://jsonplaceholder.typicode.com/posts',
    '-e', 'CURRENT_BRANCH=master',
    '-e', 'RECENT_COMMIT_MESSAGE=dependency updated',
    docker_image,
  ]

  result = subprocess.run(args, check=False, text=True, capture_output=True)
  assert 'master' in result.stdout
  assert result.returncode == 0

