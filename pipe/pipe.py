from bitbucket_pipes_toolkit import Pipe, get_logger
from requests import post

logger = get_logger()

schema = {
  'API_KEY': {'type': 'string', 'required': True},
  'API_URL': {'type': 'string', 'required': True},
  'CURRENT_BRANCH': {'type': 'string', 'required': True},
  'RECENT_COMMIT_MESSAGE': {'type': 'string', 'required': True},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}

class TriggerApi(Pipe):
    def trigger(self, url, key, message, branch):
      body = {"message": message, "branch": branch}
      customHeaders = {'Content-Type': 'application/json', 'Accept': 'application/json', 'X-Key': key}

      resp = post(url, json=body, headers=customHeaders)

      logger.warning('Completed with StatusCode:- ' + str(resp.status_code))
      logger.info('Body received:- \n' + resp.text)
    
    def run(self):
        super().run()

        logger.info('Executing the pipe...')
        
        apiUrl = self.get_variable('API_URL')
        apiKey = self.get_variable('API_KEY')
        recentCommitMessage = self.get_variable('RECENT_COMMIT_MESSAGE') # git log -1 --format=%B
        currentBranch = self.get_variable('CURRENT_BRANCH') # git branch --show-current

        print(currentBranch)

        self.trigger(url=apiUrl, key=apiKey, message=recentCommitMessage, branch=currentBranch)

        self.success(message="Success!")

if __name__ == '__main__':
    pipe = TriggerApi(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
